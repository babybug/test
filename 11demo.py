# 迭代器对象可以使用常规佛如语句进行遍历
# iter()方法
print("iter()方法")
list = [1,2,3,4]

it = iter(list)
for x in it:
    print(x, end=" ")
