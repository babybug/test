'''
pass是空语句，为了保持程序结构的完整性
pass语句一般不做任何事，一般用做占位语句
'''
for letter in 'Runoob':
    if letter == 'o':
        pass
        print("执行pass块")
    print('当前字母: ', letter)
