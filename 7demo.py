# 生成遍历数字数字序列，可以使用range()函数
# 生成数列
for i in range(5):
    print(i)

# 生成指定区间的值
for n in range(5,9):
    print(n)
# 使用range以指定数字开始并指定不同增量(甚至可以是负数，有时也叫步长)
for m in range(0, 10, 3):
    print(m)
# 可以结合range和len函数以遍历一个序列的索引
a = ["Google", "baidu", "Bing", "Yahoo"]
for l in range(len(a)):
    print(l, a[l])