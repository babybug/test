'''
break可以跳出for和while循环体，
如果你从for和while循环中终止,
任何对应循环else快将不会执行.
'''
for letter in "Runoob":
    if letter == 'b':
        break
    print("当前字母为： ", letter)

var = 10
while var > 0:
    print("当前变量为: ", var)
    var = var -1
    if var == 5:
        break

